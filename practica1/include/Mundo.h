// Mundo.h: interface for the CMundo class.
// Cabecera modificada por Irene de la Calle Pobre
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#define MAX 100 

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	//añado lo siguiente para el logger
	char cadena_mundo [MAX];
	int fd_mundo;

	//añado lo siguiente para el bot
	int fd_datos;
	DatosMemCompartida datos; //saber cuanto ocupa el espacio que tengo que reservar
	DatosMemCompartida *pdatos; //reservar la memoria
	char *org;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
